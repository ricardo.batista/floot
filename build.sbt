Global / onChangedBuildSource := ReloadOnSourceChanges

ThisBuild / organization := "com.floot"
ThisBuild / scalaVersion := "2.13.1"
ThisBuild / scalacOptions += "-deprecation"
ThisBuild / version := "0.1.0-SNAPSHOT"

lazy val akkaVersion = "2.6.3"
lazy val akkaManagementiVersion = "1.0.5"
lazy val akkaHttpVersion = "10.1.11"
lazy val scalaTestVersion = "3.0.8"
lazy val logbackVersion = "1.2.3"
lazy val upickleVersion = "0.9.5"

lazy val root = (project in file("."))
  .settings(
    name := "blog",
    maintainer := "Ricardo Batista <emacs.copo@gmail.com>",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,
      "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion,
      "ch.qos.logback" % "logback-classic" % logbackVersion,
      "com.lihaoyi" %% "upickle" % upickleVersion,
      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % "test",
      "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % "test",
      "com.typesafe.akka" %% "akka-actor-testkit-typed" % akkaVersion % "test",
      "org.scalactic" %% "scalactic" % scalaTestVersion % "test",
      "org.scalatest" %% "scalatest" % scalaTestVersion % "test"
    )
  )
  .enablePlugins(SbtTwirl)
  .enablePlugins(JavaAppPackaging)
