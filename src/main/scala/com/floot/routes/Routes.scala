package com.floot.routes

import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.model.ContentTypes
import akka.actor.typed.ActorSystem

object Routes {
  def apply(system: ActorSystem[Nothing]): Route = pathEndOrSingleSlash {
    complete("Hello, world!")
  }
}

