package com.floot

import java.io.File
import scala.io.Source
import scala.util.Failure
import scala.util.Success
import scala.util.Try

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors

object PostMeta {

  sealed trait PostMetaCommand
  final case class ParseFile(file: File, replyTo: ActorRef[PostMetaEvent])
      extends PostMetaCommand

  sealed trait PostMetaEvent
  final case class ParsedFile(meta: Option[Meta]) extends PostMetaEvent

  import upickle.default.{ReadWriter => RW, macroRW, read}
  case class Meta(
      title: String,
      tags: Set[String],
      authors: Set[String],
      posted: String,
      updated: String
  )

  object Meta {
    implicit val rw: RW[Meta] = macroRW
  }

  // TODO: Maybe check if the file is valid
  // The files that reach here should already been check
  // There should also be a away to not read the all file
  def parseFile(file: File): Option[Meta] = {
    val lines = for {
      lines <- Source.fromFile(file).getLines() if lines.startsWith("*")
    } yield lines.drop(1)

    val content = lines.foldLeft("")(_ + _)

    Try(read[Meta](s"$content")) match {
      case Success(meta) => Option(meta)
      case _             => None
    }
  }

  def apply(): Behavior[PostMetaCommand] = {
    Behaviors.setup { context =>
      Behaviors.receiveMessage {
        case ParseFile(file, replyTo) =>
          replyTo ! ParsedFile(parseFile(file))
          Behaviors.same
      }
    }
  }
}
