package com.floot

import java.io.File
import scala.io.Source

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors

object Post {
  sealed trait PostCommand
  case class Read(file: File, replyTo: ActorRef[PostEvent]) extends PostCommand

  sealed trait PostEvent
  case class Readed(content: String) extends PostEvent

  def readFile(file: File): String = {
    val lines = for {
      lines <- Source.fromFile(file).getLines() if !lines.startsWith("*")
    } yield lines

    lines.foldLeft("")(_ + _)
  }

  def apply(): Behavior[PostCommand] = {
    Behaviors.setup { context =>
      Behaviors.receiveMessage {
        case Read(file, replyTo) =>
          replyTo ! Readed(readFile(file))
          Behaviors.same
      }
    }
  }
}
