package com.floot

import akka.actor.typed.ActorSystem
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import com.floot.http.Server
import com.floot.routes.Routes

object Main extends App {
  object Guardian {
    def apply(): Behavior[Nothing] = {
      Behaviors.setup[Nothing] { context =>
        Behaviors.empty
      }
    }
  }

  val system = ActorSystem[Nothing](Guardian(), "Blog")
  new Server(Routes(system), system).start()
}
