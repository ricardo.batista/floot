package com.floot

import java.io.File

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import com.typesafe.config.ConfigFactory

object Files {

  sealed trait FilesCommand
  final case class ListFiles(path: String, replyTo: ActorRef[FilesEvent])
      extends FilesCommand
  final case class GetFile(path: String, replyTo: ActorRef[FilesEvent])
      extends FilesCommand

  sealed trait FilesEvent
  final case class FilesListed(files: List[File]) extends FilesEvent
  final case class GotFile(file: Option[File]) extends FilesEvent

  val validPath: String =
    new File(ConfigFactory.load().getString("blog.posts.path"))
      .getAbsolutePath()

  def files(path: String): List[File] = {
    val dir = new File(path)
    if (dir.exists() && dir
          .isDirectory() && dir.getAbsolutePath().startsWith(validPath)) {
      dir.listFiles().filter(_.isFile()).toList
    } else {
      List[File]()
    }
  }

  def file(path: String): Option[File] = {
    val file = new File(path)
    if (file.exists() && file
          .isFile() && file.getAbsolutePath().startsWith(validPath)) {
      Some(file)
    } else {
      None
    }
  }

  def apply(): Behavior[FilesCommand] = {
    Behaviors.setup { context =>
      Behaviors.receiveMessage {
        case ListFiles(path, replyTo) =>
          replyTo ! FilesListed(files(path))
          Behaviors.same
        case GetFile(path, replyTo) =>
          replyTo ! GotFile(file(path))
          Behaviors.same
      }
    }
  }
}
