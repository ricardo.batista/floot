package com.floot

import java.io.File

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import com.typesafe.config.ConfigFactory
import org.scalatest.BeforeAndAfterAll
import org.scalatest.Matchers
import org.scalatest.WordSpec

class PostSpec extends WordSpec with BeforeAndAfterAll with Matchers {
  val testkit = ActorTestKit()
  val config = ConfigFactory.load()
  val path = config.getString("blog.posts.path")

  "The Post Actor " must {
    val post = testkit.spawn(Post(), "post")
    val file1 = new File(s"$path/test1.md")
    val file2 = new File(s"$path/test2.md")
    val probe = testkit.createTestProbe[Post.PostEvent]()

    "be abble to get the text from a file" in {
      post ! Post.Read(file1, probe.ref)
      probe.expectMessage(Post.Readed("Some Text 1"))
      post ! Post.Read(file2, probe.ref)
      probe.expectMessage(Post.Readed("Some Text 2"))
    }
  }
}
