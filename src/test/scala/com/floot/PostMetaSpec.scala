package com.floot

import java.io.File

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import com.floot.PostMeta.Meta
import com.typesafe.config.ConfigFactory
import org.scalatest.BeforeAndAfterAll
import org.scalatest.Matchers
import org.scalatest.WordSpec

class PostMetaSpec extends WordSpec with BeforeAndAfterAll with Matchers {
  val testkit = ActorTestKit()
  val config = ConfigFactory.load()
  val path = config.getString("blog.posts.path")

  "The PostMeta Actor" must {
    val postMeta = testkit.spawn(PostMeta(), "postMeta")
    val file1 = new File(s"$path/test1.md")
    val file2 = new File(s"$path/test2.md")
    val probe = testkit.createTestProbe[PostMeta.PostMetaEvent]()

    "be able to parse the post meta from a file" in {
      postMeta ! PostMeta.ParseFile(file1, probe.ref)
      val meta = Meta(
        "A title",
        Set("Tag 1", "Tag 2"),
        Set("Author 1", "Author 2"),
        "10/01/2020",
        "10/01/2020"
      )
      probe.expectMessage(PostMeta.ParsedFile(Option(meta)))
    }

    "return none if the file doesn't have a header" in {
      postMeta ! PostMeta.ParseFile(file2, probe.ref)
      probe.expectMessage(PostMeta.ParsedFile(None))
    }

  }

  override def afterAll(): Unit = testkit.shutdownTestKit()
}
