package com.floot

import java.io.File

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import com.typesafe.config.ConfigFactory
import org.scalatest.BeforeAndAfterAll
import org.scalatest.Matchers
import org.scalatest.WordSpec

class FilesSpec extends WordSpec with BeforeAndAfterAll with Matchers {
  val testkit = ActorTestKit()
  val config = ConfigFactory.load()
  val path = config.getString("blog.posts.path")

  "The Files actor" must {
    val files = testkit.spawn(Files(), "files")
    val file1 = new File(s"$path/test1.md")
    val file2 = new File(s"$path/test2.md")
    val probe = testkit.createTestProbe[Files.FilesEvent]()
    "be able to list all files in a folder" in {
      files ! Files.ListFiles(path, probe.ref)
      probe.expectMessage(Files.FilesListed(List[File](file1, file2)))
    }
    "show an empty file list when the path is within the config file path but invalid" in {
      files ! Files.ListFiles(s"$path/invalid", probe.ref)
      probe.expectMessage(Files.FilesListed(List[File]()))
    }
    "show an empty file list when the path outside of the valid path" in {
      files ! Files.ListFiles("/invalid", probe.ref)
      probe.expectMessage(Files.FilesListed(List[File]()))
    }
    "get an option file if the file exists and its on the valid path" in {
      files ! Files.GetFile(s"$path/test1.md", probe.ref)
      probe.expectMessage(Files.GotFile(Option(file1)))
    }
    "get none if the file doesn't exist" in {
      files ! Files.GetFile(s"$path/test3.md", probe.ref)
      probe.expectMessage(Files.GotFile(None))
    }
  }

  override def afterAll(): Unit = testkit.shutdownTestKit()
}
